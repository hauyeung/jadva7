/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jadva7;

import java.awt.Graphics;
import javax.swing.JApplet;

/**
 *
 * @author Ho Hang
 */
public class circle extends JApplet {

    private javax.swing.JButton btnCircum;
    private javax.swing.JLabel lblCircum;
    private javax.swing.JTextField txtCircum;
    public int circum = 0;

    /**
     * Initialization method that will be called after the applet is loaded into
     * the browser.
     */
    public void init() {
        // TODO start asynchronous download of heavy resources
        txtCircum = new javax.swing.JTextField();
        btnCircum = new javax.swing.JButton();
        lblCircum = new javax.swing.JLabel();

        btnCircum.setText("Enter");
        btnCircum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCircumActionPerformed(evt);
            }
        });

        lblCircum.setText("Circumference:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap(38, Short.MAX_VALUE)
                        .addComponent(lblCircum, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtCircum, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnCircum)
                        .addGap(56, 56, 56))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtCircum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnCircum)
                                .addComponent(lblCircum))
                        .addContainerGap(258, Short.MAX_VALUE))
        );
    }

    private void btnCircumActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        //get circumference from input
        String cf = txtCircum.getText();
        circum = Integer.parseInt(cf);
        repaint();
    }

    // TODO overwrite start(), stop() and destroy() methods
    public void paint(Graphics g) {
        //draw circle
        super.paint(g);
        int x = 100, y = 100;
        int width = (int) (circum / (2 * Math.PI));
        int height = width;
        g.drawOval(x, y, width, height);

    }
}
